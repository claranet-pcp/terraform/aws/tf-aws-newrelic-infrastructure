# tf-aws-newrelic-infrastructure

Module to create NewRelic Infrastructure Role & Policies


## Terraform version compatibility

| Module version | Terraform version |
|----------------|-------------------|
| 2.x.x          | 0.15.x            |
| 1.x.x          | > 0.12.x          |
| 0.x.x          | 0.11.x            |


## Usage

```
module "NewRelicInfra" {
  source               = "git@gogs.bashton.net:Bashton-Terraform-Modules/tf-aws-newrelic-infrastructure.git"
  new_relic_account_id = "0123456789ABCDEF"

  ec2                  = true
  elb                  = true
  health               = true
  iam                  = true
  rds                  = true
  route53              = true
  s3                   = true
  sns                  = true
  vpc                  = true
}
```

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-----:|:-----:|
| alb | Enable alb access | string | `false` | no |
| api_gateway | Enable api_gateway access | string | `false` | no |
| autoscaling | Enable autoscaling access | string | `false` | no |
| beanstalk | Enable beanstalk access | string | `false` | no |
| billing | Enable billing access | string | `false` | no |
| cloudfront | Enable cloudfront access | string | `false` | no |
| cloudtrail | Enable cloudtrail access | string | `false` | no |
| dynamodb | Enable dynamodb access | string | `false` | no |
| ebs | Enable ebs access | string | `false` | no |
| ec2 | Enable ec2 access | string | `false` | no |
| ecs | Enable ecs access | string | `false` | no |
| efs | Enable efs access | string | `false` | no |
| elasticache | Enable elasitcache access | string | `false` | no |
| elasticsearch | Enable elasticsearch access | string | `false` | no |
| elb | Enable elb access | string | `false` | no |
| emr | Enable emr access | string | `false` | no |
| firehose | Enable kinesis_firehose access | string | `false` | no |
| health | Enable health access | string | `false` | no |
| iam | Enable iam access | string | `false` | no |
| iot | Enable iot access | string | `false` | no |
| kinesis | Enable kinesis_streams access | string | `false` | no |
| lambda | Enable lambda access | string | `false` | no |
| new_relic_account_id | New Relic Account Id | string | - | yes |
| rds | Enable rds access | string | `false` | no |
| redshift | Enable redshift access | string | `false` | no |
| route53 | Enable route53 access | string | `false` | no |
| s3 | Enable s3 access | string | `false` | no |
| ses | Enable ses access | string | `false` | no |
| sns | Enable sns access | string | `false` | no |
| sqs | Enable sqs access | string | `false` | no |
| vpc | Enable vpc access | string | `false` | no |

## Outputs

| Name | Description |
|------|-------------|
| arn | ARN for the NewRelic-Infrastructure role |
