locals {
  required = var.alb == true || var.api_gateway == true || var.autoscaling == true || var.billing == true || var.cloudfront == true || var.cloudtrail == true || var.dynamodb == true || var.ebs == true || var.ec2 == true || var.ecs == true || var.efs == true || var.elasticache == true || var.elasticsearch == true || var.beanstalk == true || var.elb == true || var.emr == true || var.health == true || var.iam == true || var.iot == true || var.firehose == true || var.kinesis == true || var.lambda == true || var.rds == true || var.redshift == true || var.route53 == true || var.s3 == true || var.ses == true || var.sns == true || var.sqs == true || var.vpc == true ? true : false
}

locals {

  NewRelic-Integration_assembly = compact(concat(
    local.NewRelic-Integration_ViewBudget,
    local.NewRelic-Integration_cloudwatch,
    split(",", local.NewRelic-Integration_alb_policy_str),
    split(",", local.NewRelic-Integration_api_gateway_policy_str),
    split(",", local.NewRelic-Integration_autoscaling_policy_str),
    split(",", local.NewRelic-Integration_billing_policy_str),
    split(",", local.NewRelic-Integration_cloudfront_policy_str),
    split(",", local.NewRelic-Integration_cloudtrail_policy_str),
    split(",", local.NewRelic-Integration_dynamodb_policy_str),
    split(",", local.NewRelic-Integration_ebs_policy_str),
    split(",", local.NewRelic-Integration_ec2_policy_str),
    split(",", local.NewRelic-Integration_ecs_policy_str),
    split(",", local.NewRelic-Integration_efs_policy_str),
    split(",", local.NewRelic-Integration_elasticache_policy_str),
    split(",", local.NewRelic-Integration_elasticsearch_policy_str),
    split(",", local.NewRelic-Integration_beanstalk_policy_str),
    split(",", local.NewRelic-Integration_elb_policy_str),
    split(",", local.NewRelic-Integration_emr_policy_str),
    split(",", local.NewRelic-Integration_health_policy_str),
    split(",", local.NewRelic-Integration_iam_policy_str),
    split(",", local.NewRelic-Integration_iot_policy_str),
    split(",", local.NewRelic-Integration_firehose_policy_str),
    split(",", local.NewRelic-Integration_kinesis_policy_str),
    split(",", local.NewRelic-Integration_lambda_policy_str),
    split(",", local.NewRelic-Integration_rds_policy_str),
    split(",", local.NewRelic-Integration_redshift_policy_str),
    split(",", local.NewRelic-Integration_route53_policy_str),
    split(",", local.NewRelic-Integration_s3_policy_str),
    split(",", local.NewRelic-Integration_ses_policy_str),
    split(",", local.NewRelic-Integration_sns_policy_str),
    split(",", local.NewRelic-Integration_sqs_policy_str),
    split(",", local.NewRelic-Integration_vpc_policy_str)
  ))

  # Always Required
  NewRelic-Integration_ViewBudget = ["budgets:ViewBudget"]

  NewRelic-Integration_cloudwatch = [
    "cloudwatch:GetMetricStatistics",
    "cloudwatch:ListMetrics",
    "cloudwatch:GetMetricData",
  ]


  NewRelic-Integration_alb_policy_str = var.alb == true ? join(",", local.NewRelic-Integration_alb_policy) : ""

  NewRelic-Integration_alb_policy = [
    "elasticloadbalancing:DescribeLoadBalancers",
    "elasticloadbalancing:DescribeTargetGroups",
    "elasticloadbalancing:DescribeTags",
    "elasticloadbalancing:DescribeLoadBalancerAttributes",
    "elasticloadbalancing:DescribeListeners",
    "elasticloadbalancing:DescribeRules",
    "elasticloadbalancing:DescribeTargetGroupAttributes",
  ]

  NewRelic-Integration_api_gateway_policy_str = var.api_gateway == true ? join(",", local.NewRelic-Integration_api_gateway_policy) : ""

  NewRelic-Integration_api_gateway_policy = [
    "apigateway:GET",
    "apigateway:HEAD",
    "apigateway:OPTION",
  ]

  NewRelic-Integration_autoscaling_policy_str = var.autoscaling == true ? join(",", local.NewRelic-Integration_autoscaling_policy) : ""

  NewRelic-Integration_autoscaling_policy = [
    "autoscaling:DescribeLaunchConfigurations",
    "autoscaling:DescribeAutoScalingGroups",
    "autoscaling:DescribePolicies",
    "autoscaling:DescribeTags",
  ]

  NewRelic-Integration_billing_policy_str = var.billing == true ? join(",", local.NewRelic-Integration_billing_policy) : ""

  NewRelic-Integration_billing_policy = [
    "aws-portal:ViewBilling",
    "aws-portal:ViewBudget",
    "aws-portal:ViewUsage",
  ]

  NewRelic-Integration_cloudfront_policy_str = var.cloudfront == true ? join(",", local.NewRelic-Integration_cloudfront_policy) : ""

  NewRelic-Integration_cloudfront_policy = [
    "cloudfront:ListDistributions",
    "cloudfront:ListStreamingDistributions",
  ]

  NewRelic-Integration_cloudtrail_policy_str = var.cloudtrail == true ? join(",", local.NewRelic-Integration_cloudtrail_policy) : ""

  NewRelic-Integration_cloudtrail_policy = [
    "cloudtrail:LookupEvents",
  ]

  NewRelic-Integration_dynamodb_policy_str = var.dynamodb == true ? join(",", local.NewRelic-Integration_dynamodb_policy) : ""

  NewRelic-Integration_dynamodb_policy = [
    "dynamodb:DescribeLimits",
    "dynamodb:ListTables",
    "dynamodb:DescribeTable",
    "dynamodb:ListGlobalTables",
  ]

  NewRelic-Integration_ebs_policy_str = var.ebs == true ? join(",", local.NewRelic-Integration_ebs_policy) : ""

  NewRelic-Integration_ebs_policy = [
    "ec2:DescribeVolumeStatus",
    "ec2:DescribeVolumes",
    "ec2:DescribeVolumeAttribute",
  ]

  NewRelic-Integration_ec2_policy_str = var.ec2 == true ? join(",", local.NewRelic-Integration_ec2_policy) : ""

  NewRelic-Integration_ec2_policy = [
    "ec2:DescribeInstanceStatus",
    "ec2:DescribeInstances",
  ]

  NewRelic-Integration_ecs_policy_str = var.ecs == true ? join(",", local.NewRelic-Integration_ecs_policy) : ""

  NewRelic-Integration_ecs_policy = [
    "ecs:ListServices",
    "ecs:DescribeServices",
    "ecs:DescribeClusters",
    "ecs:ListClusters",
  ]

  NewRelic-Integration_efs_policy_str = var.efs == true ? join(",", local.NewRelic-Integration_efs_policy) : ""

  NewRelic-Integration_efs_policy = [
    "elasticfilesystem:DescribeMountTargets",
    "elasticfilesystem:DescribeFileSystems",
  ]

  NewRelic-Integration_elasticache_policy_str = var.elasticache == true ? join(",", local.NewRelic-Integration_elasticache_policy) : ""

  NewRelic-Integration_elasticache_policy = [
    "elasticache:DescribeCacheClusters",
  ]

  NewRelic-Integration_elasticsearch_policy_str = var.elasticsearch == true ? join(",", local.NewRelic-Integration_elasticsearch_policy) : ""

  NewRelic-Integration_elasticsearch_policy = [
    "es:ListDomainNames",
    "es:DescribeElasticsearchDomain",
    "es:DescribeElasticsearchDomains",
  ]

  NewRelic-Integration_beanstalk_policy_str = var.beanstalk == true ? join(",", local.NewRelic-Integration_beanstalk_policy) : ""

  NewRelic-Integration_beanstalk_policy = [
    "elasticbeanstalk:DescribeEnvironments",
    "elasticbeanstalk:DescribeInstancesHealth",
  ]

  NewRelic-Integration_elb_policy_str = var.elb == true ? join(",", local.NewRelic-Integration_elb_policy) : ""

  NewRelic-Integration_elb_policy = [
    "elasticloadbalancing:DescribeLoadBalancers",
  ]

  NewRelic-Integration_emr_policy_str = var.emr == true ? join(",", local.NewRelic-Integration_emr_policy) : ""

  NewRelic-Integration_emr_policy = [
    "elasticmapreduce:ListInstances",
    "elasticmapreduce:ListClusters",
    "elasticmapreduce:DescribeCluster",
    "elasticmapreduce:ListInstanceGroups",
  ]

  NewRelic-Integration_health_policy_str = var.health == true ? join(",", local.NewRelic-Integration_health_policy) : ""

  NewRelic-Integration_health_policy = [
    "health:DescribeAffectedEntities",
    "health:DescribeEventDetails",
    "health:DescribeEvents",
  ]

  NewRelic-Integration_iam_policy_str = var.iam == true ? join(",", local.NewRelic-Integration_iam_policy) : ""

  NewRelic-Integration_iam_policy = [
    "iam:ListSAMLProviders",
    "iam:ListOpenIDConnectProviders",
    "iam:ListServerCertificates",
    "iam:GetAccountAuthorizationDetails",
    "iam:ListVirtualMFADevices",
    "iam:GetAccountSummary",
  ]

  NewRelic-Integration_iot_policy_str = var.iot == true ? join(",", local.NewRelic-Integration_iot_policy) : ""

  NewRelic-Integration_iot_policy = [
    "iot:ListTopicRules",
    "iot:GetTopicRule",
    "iot:ListThings",
  ]

  NewRelic-Integration_firehose_policy_str = var.firehose == true ? join(",", local.NewRelic-Integration_firehose_policy) : ""

  NewRelic-Integration_firehose_policy = [
    "firehose:DescribeDeliveryStream",
    "firehose:ListDeliveryStreams",
  ]

  NewRelic-Integration_kinesis_policy_str = var.kinesis == true ? join(",", local.NewRelic-Integration_kinesis_policy) : ""

  NewRelic-Integration_kinesis_policy = [
    "kinesis:ListStreams",
    "kinesis:DescribeStream",
    "kinesis:ListTagsForStream",
  ]

  NewRelic-Integration_lambda_policy_str = var.lambda == true ? join(",", local.NewRelic-Integration_lambda_policy) : ""

  NewRelic-Integration_lambda_policy = [
    "lambda:GetAccountSettings",
    "lambda:ListFunctions",
    "lambda:ListAliases",
    "lambda:ListTags",
    "lambda:ListEventSourceMappings",
  ]

  NewRelic-Integration_rds_policy_str = var.rds == true ? join(",", local.NewRelic-Integration_rds_policy) : ""

  NewRelic-Integration_rds_policy = [
    "rds:ListTagsForResource",
    "rds:DescribeDBInstances",
    "rds:DescribeDBClusters",
  ]

  NewRelic-Integration_redshift_policy_str = var.redshift == true ? join(",", local.NewRelic-Integration_redshift_policy) : ""

  NewRelic-Integration_redshift_policy = [
    "redshift:DescribeClusters",
    "redshift:DescribeClusterParameters",
  ]

  NewRelic-Integration_route53_policy_str = var.route53 == true ? join(",", local.NewRelic-Integration_route53_policy) : ""

  NewRelic-Integration_route53_policy = [
    "route53:ListHealthChecks",
    "route53:GetHostedZone",
    "route53:ListHostedZones",
    "route53:ListResourceRecordSets",
  ]

  NewRelic-Integration_s3_policy_str = var.s3 == true ? join(",", local.NewRelic-Integration_s3_policy) : ""

  NewRelic-Integration_s3_policy = [
    "s3:GetLifecycleConfiguration",
    "s3:GetBucketTagging",
    "s3:ListAllMyBuckets",
    "s3:GetBucketWebsite",
    "s3:GetBucketLogging",
    "s3:GetBucketCORS",
    "s3:GetBucketVersioning",
    "s3:GetBucketAcl",
    "s3:GetBucketNotification",
    "s3:GetBucketPolicy",
    "s3:GetReplicationConfiguration",
    "s3:GetMetricsConfiguration",
  ]

  NewRelic-Integration_ses_policy_str = var.ses == true ? join(",", local.NewRelic-Integration_ses_policy) : ""

  NewRelic-Integration_ses_policy = [
    "ses:ListConfigurationSets",
    "ses:GetSendQuota",
    "ses:DescribeConfigurationSet",
    "ses:ListReceiptRuleSets",
    "ses:DescribeReceiptRule",
  ]

  NewRelic-Integration_sns_policy_str = var.sns == true ? join(",", local.NewRelic-Integration_sns_policy) : ""

  NewRelic-Integration_sns_policy = [
    "sns:GetTopicAttributes",
    "sns:ListTopics",
  ]

  NewRelic-Integration_sqs_policy_str = var.sqs == true ? join(",", local.NewRelic-Integration_sqs_policy) : ""

  NewRelic-Integration_sqs_policy = [
    "sqs:ListQueues",
    "sqs:GetQueueAttributes",
  ]

  NewRelic-Integration_vpc_policy_str = var.vpc == true ? join(",", local.NewRelic-Integration_vpc_policy) : ""

  NewRelic-Integration_vpc_policy = [
    "ec2:DescribeInternetGateways",
    "ec2:DescribeVpcs",
    "ec2:DescribeNatGateways",
    "ec2:DescribeVpcEndpoints",
    "ec2:DescribeSubnets",
    "ec2:DescribeNetworkAcls",
    "ec2:DescribeVpcAttribute",
    "ec2:DescribeRouteTables",
    "ec2:DescribeSecurityGroups",
  ]
}
