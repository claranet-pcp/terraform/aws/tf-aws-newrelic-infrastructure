resource "aws_iam_role" "NewRelicInfrastructure-Integrations" {
  count = local.required == true ? 1 : 0

  name = "NewRelicInfrastructure-Integrations"
  path = "/"

  assume_role_policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "AWS": "arn:aws:iam::754728514883:root"
      },
      "Action": "sts:AssumeRole",
      "Condition": {
        "StringEquals": {
          "sts:ExternalId": "${var.new_relic_account_id}"
        }
      }
    }
  ]
}
POLICY
}

resource "aws_iam_role_policy" "NewRelicInfrastructure-Integrations" {
  count = local.required == true ? 1 : 0

  name = "NewRelicInfrastructure-Integrations"
  role = aws_iam_role.NewRelicInfrastructure-Integrations[0].name

  policy = data.aws_iam_policy_document.NewRelic-Integration_access_policy[0].json
}

data "aws_iam_policy_document" "NewRelic-Integration_access_policy" {
  count = local.required == true ? 1 : 0

  statement {
    actions   = local.NewRelic-Integration_assembly
    effect    = "Allow"
    resources = ["*"]
  }
}
