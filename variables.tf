variable "new_relic_account_id" {
  type        = string
  description = "New Relic Account Id"
}

variable "alb" {
  description = "Enable alb access"
  default     = false
}

variable "api_gateway" {
  description = "Enable api_gateway access"
  default     = false
}

variable "autoscaling" {
  description = "Enable autoscaling access"
  default     = false
}

variable "billing" {
  description = "Enable billing access"
  default     = false
}

variable "cloudfront" {
  description = "Enable cloudfront access"
  default     = false
}

variable "cloudtrail" {
  description = "Enable cloudtrail access"
  default     = false
}

variable "dynamodb" {
  description = "Enable dynamodb access"
  default     = false
}

variable "ebs" {
  description = "Enable ebs access"
  default     = false
}

variable "ec2" {
  description = "Enable ec2 access"
  default     = false
}

variable "ecs" {
  description = "Enable ecs access"
  default     = false
}

variable "efs" {
  description = "Enable efs access"
  default     = false
}

variable "elasticache" {
  description = "Enable elasitcache access"
  default     = false
}

variable "elasticsearch" {
  description = "Enable elasticsearch access"
  default     = false
}

variable "beanstalk" {
  description = "Enable beanstalk access"
  default     = false
}

variable "elb" {
  description = "Enable elb access"
  default     = false
}

variable "emr" {
  description = "Enable emr access"
  default     = false
}

variable "health" {
  description = "Enable health access"
  default     = false
}

variable "iam" {
  description = "Enable iam access"
  default     = false
}

variable "iot" {
  description = "Enable iot access"
  default     = false
}

variable "firehose" {
  description = "Enable kinesis_firehose access"
  default     = false
}

variable "kinesis" {
  description = "Enable kinesis_streams access"
  default     = false
}

variable "lambda" {
  description = "Enable lambda access"
  default     = false
}

variable "rds" {
  description = "Enable rds access"
  default     = false
}

variable "redshift" {
  description = "Enable redshift access"
  default     = false
}

variable "route53" {
  description = "Enable route53 access"
  default     = false
}

variable "s3" {
  description = "Enable s3 access"
  default     = false
}

variable "ses" {
  description = "Enable ses access"
  default     = false
}

variable "sns" {
  description = "Enable sns access"
  default     = false
}

variable "sqs" {
  description = "Enable sqs access"
  default     = false
}

variable "vpc" {
  description = "Enable vpc access"
  default     = false
}
