output "arn" {
  description = "ARN for the NewRelic-Infrastructure role"
  value = element(concat(aws_iam_role.NewRelicInfrastructure-Integrations.*.arn, tolist([""])), 0)
}
